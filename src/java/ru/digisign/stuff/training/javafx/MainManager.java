package ru.digisign.stuff.training.javafx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;

import java.io.IOException;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author dpetuhov
 */
public class MainManager {

    Scene scene;

    public MainManager(Scene scene) {
        this.scene = scene;
    }

    public void showMainView() {
        Locale locale = Locale.getDefault();//new Locale("en", "UK");
        ResourceBundle bundle = ResourceBundle.getBundle("strings", locale);

        try {
            scene.setRoot(FXMLLoader.load(getClass().getResource("main.fxml"), bundle));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
