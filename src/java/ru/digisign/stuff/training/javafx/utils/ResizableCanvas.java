package ru.digisign.stuff.training.javafx.utils;

import javafx.scene.canvas.Canvas;

/**
 * @author dpetuhov
 */
public class ResizableCanvas extends Canvas {

    @Override
    public boolean isResizable() {
        return true;
    }

    @Override
    public double prefWidth(double height) {
        return getWidth();
    }

    @Override
    public double prefHeight(double width) {
        return getHeight();
    }

    @Override
    public double minWidth(double height) {
        return 0;
    }

    @Override
    public double minHeight(double width) {
        return 0;
    }

    @Override
    public double maxWidth(double height) {
        return getWidth();
    }

    @Override
    public double maxHeight(double width) {
        return getHeight();
    }
}

