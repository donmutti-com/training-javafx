package ru.digisign.stuff.training.javafx;

import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    private final Border BORDER = new Border(
        new BorderStroke(
            Color.RED,
            new BorderStrokeStyle(
                StrokeType.INSIDE,
                StrokeLineJoin.ROUND,
                StrokeLineCap.SQUARE,
                10,
                0,
                new ArrayList<Double>(Arrays.asList(4.0, 2.0)) // null
            ),
            null,//new CornerRadii(0.0),
            new BorderWidths(0.6),
            null//new Insets(0, 0, 0, 0)
        )
    );
    private ListChangeListener<? super Node> BORDER_LISTENER = new BorderListener();

    @FXML VBox root;
    @FXML private Canvas canvas;
    @FXML private Button btnClear;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        overrideBorderRecursively(root);

        repaint();

        canvas.widthProperty().bind(root.widthProperty());
        canvas.heightProperty().bind(root.heightProperty());
        canvas.widthProperty().addListener(observable -> repaint());
        canvas.heightProperty().addListener(observable -> repaint());

    }

    private void repaint() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.LIGHTSKYBLUE);
        gc.setStroke(Color.BLANCHEDALMOND);
        gc.setLineWidth(3.0);
        gc.strokeRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getWidth());
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getWidth());
        gc.strokeOval(20, 20, 100, 100);

    }

    private void clear() {
        GraphicsContext gc = canvas.getGraphicsContext2D();
        gc.setFill(Color.WHITE);
        gc.fillRect(0, 0, gc.getCanvas().getWidth(), gc.getCanvas().getWidth());

    }

    private void overrideBorderRecursively(Node node) {
        if (node instanceof Region) {
            ((Region) node).setBorder(BORDER);
        }
        if (node instanceof Parent) {
            ((Parent) node).getChildrenUnmodifiable().addListener(BORDER_LISTENER);
            for (Node child : ((Parent) node).getChildrenUnmodifiable()) {
                overrideBorderRecursively(child);
            }
        }
    }

    public void onClear(ActionEvent actionEvent) {
        clear();
    }

    private class BorderListener implements ListChangeListener<Node> {
        @Override
        public void onChanged(Change<? extends Node> change) {
            if (change.next() && change.wasAdded()) {
                List<? extends Node> addedNodes = change.getAddedSubList();
                for (Node node : addedNodes) {
                    overrideBorderRecursively(node);
                }
            }
        }
    }

}
